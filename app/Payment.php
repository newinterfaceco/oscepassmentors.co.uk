<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'name',
        'date',
        'phone',
        'email',
        'price',
        'type',
        'status',
        'user_message'
    ];
}
