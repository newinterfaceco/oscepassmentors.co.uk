<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mail\ContactRequest;
use App\Mail\PaymentRequest;
use App\Http\Requests\ContactFormRequest;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

class ContactController extends Controller
{

	private $api_context;

    public function __construct()
    {
        $this->api_context = new ApiContext(new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret')));
        $this->api_context->setConfig(config('paypal.settings'));
    }

	public function request(ContactFormRequest $request)
    {

    	Mail::to('info@oscepassmentors.co.uk')->send(new ContactRequest($request));

	    Mail::to('adam@newinterface.co')->send(new ContactRequest($request));

    	return back()->with('contact_message', 'Thanks for contacting us!');
	}
	
	public function pay(Request $request)
	{
		$payment = \App\Payment::make($request->all());

		$payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $itemList = new ItemList();

		$item = new Item();

		if ($request->type == 'bespoke') {

			$cost = 129;
			$type = 'Bespoke';

		} else {

			$cost = 229;
			$type = 'Group';

		}

		$item->setName($type . ' OSCE Mentoring')
			->setCurrency('GBP')
			->setQuantity(1)
			->setSku(1) // Similar to `item_number` in Classic API
			->setPrice($cost);

			$itemList->addItem($item);

			$details = new Details();
			$details->setTax(0)
					->setSubtotal($cost);

			$amount = new Amount();
			$amount->setCurrency("GBP")
				->setTotal($cost)
				->setDetails($details);

			$transaction = new Transaction();
			$transaction->setAmount($amount)
						->setItemList($itemList)
						->setDescription($type . ' OSCE Mentoring');

			$payment->price = $cost;
			$payment->type = $type;
			$payment->status = 'pending';

		$payment->save();
                    
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(env('APP_URL') . '/' . $payment->id . '/payment-success')
                      ->setCancelUrl(env('APP_URL') . '/' . $payment->id . '/payment-error');
                      
        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions([$transaction]);

        try {
            $payment->create($this->api_context);
        } catch (PayPalConnectionException $ex){
            return response()->json(['message' => json_decode($ex->getData())], 422);
        } catch (Exception $ex) {
            return response()->json(['message' => json_decode($ex->getData())], 422);
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
		}
		
		header("Location: " . $redirect_url);
		die();

		return [];
	}

	public function pay_success($payment_id, Request $request)
	{
        // If query data not available... no payments was made.
        if (!$request->has('PayerID') || !$request->has('paymentId') || !$request->has('token')) {
		   // redirect back and error
		   return redirect('/')->with('payment_error_message', 'Payment unsuccessful!');
		}
		
		try{
				
			// We retrieve the payment from the paymentId.
			$payment = Payment::get($request->get('paymentId'), $this->api_context);

			// We create a payment execution with the PayerId
			$execution = new PaymentExecution();
			$execution->setPayerId($request->get('PayerID'));
	
			// Then we execute the payment.
			$result = $payment->execute($execution, $this->api_context);

			$payment = \App\Payment::find($payment_id);

			Mail::to($payment->email)->send(new PaymentRequest($payment));

			Mail::to('info@oscepassmentors.co.uk')->send(new PaymentRequest($payment));

			Mail::to('adam@newinterface.co')->send(new PaymentRequest($payment));
			
			$payment->status = 'complete';
			$payment->save();

		} catch(PayPalConnectionException $e){
			echo $e->getCode(); // Prints the Error Code
			echo $e->getData();
			die($e);
		} catch (Exception $ex) {
			die($ex);
		}

		// payment success

		return redirect('/')->with('payment_success_message', 'Payment successful! Please check your email for a confirmation email.');
	}

	public function pay_error()
	{
		return redirect('/')->with('payment_error_message', 'Payment unsuccessful!');
	}
}