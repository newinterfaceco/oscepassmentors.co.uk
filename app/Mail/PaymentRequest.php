<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payment)
    {
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.payment')->with([
            'name' => $this->payment->name,
            'email' => $this->payment->email,
            'phone' => $this->payment->phone,
            'date' => $this->payment->date,
            'price' => $this->payment->price,
            'type' => $this->payment->type,
            'user_message' => $this->payment->user_message
        ])->replyTo('info@oscepassmentors.co.uk', 'OSCE PASS MENTORS')->subject('Booking Successful');
    }
}