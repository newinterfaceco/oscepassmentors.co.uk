<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact')->with([
            'name' => $this->request->name,
            'subject' => $this->request->subject,
            'email' => $this->request->email,
            'phone' => $this->request->phone,
            'user_message' => $this->request->user_message,
        ])->replyTo($this->request->email, $this->request->name)->subject($this->request->subject);
    }
}