<!doctype html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136546656-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-136546656-1');
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>OSCE PASS MENTORS</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/swipebox.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cardo" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Unna" rel="stylesheet">
</head>
<body>

    <!-- @if(Session::has('payment_success_message')) -->
        <div class="container">
            <div class="alert alert-success" style="width:100%;" role="alert">
                Payment successful! Please check your email for a confirmation email.
            </div>
        </div>
    <!-- @endif -->

    @if(Session::has('payment_error_message'))
        <div class="container">
            <div class="alert alert-danger" style="width:100%;" role="alert">
                Payment unsuccessful!
            </div>
        </div>
    @endif

    <nav class="navbar bg-white" style="position: absolute;top: 0;width: 100%;">
        <div class="container">
            <div class="logo" style="margin-bottom:20px;display: inline-block;float:left;margin:0;"><img src="/logo.png" style="width:300px;"></div>
            <div>
                <p class="contact-details" style="display: inline-block;font-size: 22px;text-align: right;color:#2e4e82;line-height:1.5;">
                    <i class="material-icons" style="color:#234f86;position:relative;top:5px;margin-right:10px;">phone</i>07932 750507<br>
                    <i class="material-icons" style="color:#234f86;position:relative;top:5px;margin-right:10px;">email</i>info@oscepassmentors.co.uk</p>
                <a target="_blank" href="https://www.linkedin.com/in/steve-shiri-gill-27937bab/?originalSubdomain=uk">
                    <img style="width: 55px;margin-left: 15px;position: relative;top: -20px;" src="/linkedin.png"> <span style="color:#000;position:relative;top:-15px;text-decoration:none!important;">View my linkedin</span>
                </a>
            </div>
        </div>
    </nav>

    <div class="container">
        <img src="/banner.jpg" style="width:100%;margin-top:240px;">
    </div>

    <div class="container" style="display:none;">
        <div class="banner">
            <div class="overlay" style="padding: 50px 0;">
                <h1 style="font-size:24px;padding: 0 15px;    max-width: 650px;
    margin: 0 auto 30px;">We transform lives by delivering knowledge, communication skills and clinical practice under exam conditions for your OSCE exam success.</h1>
                <h2 style="color: #fff;font-size: 18px;text-align: right; max-width: 850px;padding-right:15px;
    margin: 0 auto 5px;">Steve Shiri Gill (L) RAMC MA EN RN Clin Ed. PA BSc MSc PA(V)R</h2>
                <h2 style="color: #fff;font-size: 18px;text-align: right;padding-right: 130px;">Founder OSCE PASS MENTORS</h2>
            </div>
        </div>
    </div>

    <section id="welcome-s" style="padding: 0;">
      <div class="container">
        
        <div class="justify-content-md-center">
            <div class="intro-welcome" style="padding: 50px 0;border: 3px solid #234f86;background-color: #234f86;">
                <p style="text-align: center;    color: #fff;
    font-family: 'Cardo', sans-serif;
    text-align: center;
    margin-bottom: 50px;
    margin-top: 50px;
    font-size: 22px;
    letter-spacing: 1px;
    padding: 0 15px;
    max-width: 715px;
    margin: 0 auto;
    line-height: 50px;">
                {{--Welcome to my site, I provide simple and to the point clinical training and feedback of your practice under exam conditions including OSCE exam pass.--}}
                    Welcome to my site,<br> I am a UK trained Clinical Trainer with over 25 years experience, Advanced communication coach and national OSCE examiner. I understand the required preparation for national OSCE exam.
            </div>
          </div>
        </div>
      
    </section>

    <section id="welcome-s" style="padding: 0;">
        <div class="container">

            <div class="justify-content-md-center">
                <div class="intro-welcome" style="padding: 50px 0;border: 3px solid #649bc7;background-color: #649bc7;">
                    <p style="text-align: center;    color: #fff;
    font-family: 'Cardo', sans-serif;
    text-align: center;
    margin-bottom: 50px;
    margin-top: 50px;
    font-size: 22px;
    letter-spacing: 1px;
    padding: 0 15px;
    max-width: 850px;
    margin: 0 auto;
    line-height: 50px;">
                        {{--I am a UK trained Clinical Trainer with 30 years experience, MSc Physician Associate and national OSCE examiner.<br> I understand the required preparation for national OSCE exam pass.--}}
                        We provide simple and to the point clinical training and feedback to Physician Associates and Nurse practitioners under exam conditions including preparation for national OSCE exam.
                        I take pride in your clinical development and OSCE pass and offer free training should you need to retake.
                    </p>
                </div>
            </div>
        </div>

    </section>

    {{--<section id="welcome-s" style="padding: 0;">--}}
      {{--<div class="container">--}}
        {{----}}
        {{--<div class="justify-content-md-center">--}}
            {{--<div class="intro-welcome" style="padding: 50px 0;border: 3px solid #dde7ef;">--}}
                {{--<p style="text-align: center;    color: #000;--}}
    {{--font-family: 'Cardo', sans-serif;--}}
    {{--text-align: center;--}}
    {{--margin-bottom: 50px;--}}
    {{--margin-top: 50px;--}}
    {{--font-size: 22px;--}}
    {{--letter-spacing: 1px;--}}
    {{--padding: 0 15px;--}}
    {{--max-width: 650px;--}}
    {{--margin: 0 auto;--}}
    {{--line-height: 50px;">--}}
                    {{--I take pride in your clinical development and OSCE pass and offer free training should you need to retake.--}}
                {{--</p>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{----}}
    {{--</section>--}}

    <section id="track">


        <div class="container">


            <h2 class="title" style="font-size: 40px;">The Training Program:</h2>


            <div class="row">

            <div class="col-lg-12">
                <div class="card" style="background-color: #2e4e82;border-color: #2e4e82;color:#fff;margin-bottom:0;">
                <div class="card-body">
                    {{--<h5 class="card-title">1-on-1 Training</h5>--}}
                    <p class="card-text" style="color:#f9f9f9;padding: 0;margin:0;font-size: 20px;"><span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> A designated training site for your clinical development and OSCE practice is now available near Whipps Cross Hospital, London.</p>
                    <p class="card-text" style="color:#f9f9f9;font-size: 20px;padding:0;margin-bottom:0;"><span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> The site is suitable for one-on-one training or a maximum 6 student training event including OSCE mock exam practice.</p>
                    <p class="card-text" style="color:#f9f9f9;font-size:20px;padding:0;margin:0;">
                        <span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span>  You can develop advanced communication and hx taking, procedural skills, OSCE pass skills during the week or weekends.<br> <span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> Your communication skills and clinical practice skills can be filmed as a part of your feedback process.</p>
                    <p class="card-text" style="color: #f9f9f9;font-size:20px;padding:0;margin:0;"><span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> A 2-day Physician Associate OSCE station exam under national exam conditions will be offered in January, May and September.</p>

                    {{--<h6>From £129 per day</h6>--}}
                    {{--<br>--}}
                    {{--<a href="#" class="btn btn-primary bespoke-book">Book</a>--}}
                </div>
                </div>
            </div>
            

            {{--<div class="col-lg-12">--}}
                {{--<div class="card" style="background-color: #649bc7;border-color: #649bc7;color:#fff;margin:0px;">--}}
                {{--<div class="card-body">--}}
                    {{--<p class="card-text" style="color:#f9f9f9;font-size:20px;padding:0;margin:0;">--}}
                    {{--At the site, you can develop advanced communication and history taking skills, procedural skills as well as OSCE pass skills during the week or weekends according to your need. Your communication skills and clinical practice skills can be filmed as a part of your feedback process.</p>--}}
                    {{--<h6>£229 per 2 day workshop</h6>--}}
                    {{--<br>--}}
                    {{--<a href="#" class="btn btn-primary group-book">Book</a>--}}
                {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-lg-12">--}}
                {{--<div class="card" style="background-color: #fff;border-color: #dde7ef;color:#000;margin-top:0px;padding:0;">--}}
                    {{--<div class="card-body">--}}
                        {{--<p class="card-text" style="color:#000;font-size:20px;padding:0;margin:0;">A 2-day PA OSCE station mock exam under national exam conditions will be offered in January, May and September.</p>--}}
                        {{--<h6>£229 per 2 day workshop</h6>--}}
                        {{--<br>--}}
                        {{--<a href="#" class="btn btn-primary group-book">Book</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{----}}
            
            {{--<div class="col-lg-4">--}}
                {{--<div class="card" style="background-color: #dde7ef;border-color: #dde7ef;color:#000;">--}}
                {{--<div class="card-body">--}}
                    {{--<h5 class="card-title">Institutions</h5>--}}
                    {{--<p class="card-text" style="color:#000;">--}}
                    {{--We can be booked to come to your institution and provide bespoke--}}
                    {{--training or national OSCE preparation programs.<br>--}}

                    {{--Please contact us and we can discuss how we can support your training need. </p>--}}
                    {{--<h6>Negotiable</h6>--}}
                    {{--<br>--}}
                    {{--<a href="#" class="btn btn-primary contact-scroll">Contact Us</a>--}}
                {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div> 

        </div>


        <div class="container" id="product-content"> 

            <div id="bespoke-service" style="display: block;" class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                        
                            {{--<h5 class="card-title">Bespoke</h5>--}}
                            <p class="card-text">
                                {{--This opportunity is for students who require one-on-one or up to a maximum 6 student group training preparation for OSCE pass at our designated clinical training site near Whipps Cross hospital, London.<br>--}}
                                {{--We can offer weekday or weekend training programs to suit your need. (we can come to you if you require for bespoke training)<br>--}}
                                {{--Our bespoke training blocks are tailored to your clinical practice development or preparation for national OSCE exams:<br>--}}
                                I can offer the following training in 3-hour blocks.<br><br>
                                You can choose any two blocks to be covered in a one day session subject to availability or all 6 blocks over 3 days.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#234F81;margin-right:10px;"></span> BLOCK ONE:<br> PA national exam prep and marking scheme for OSCE pass.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#649bc7;margin-right:10px;"></span> BLOCK TWO:<br> Advanced communication and presentation skill training and mock OSCE practice with certified OSCE examiner feedback.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> BLOCK THREE:<br> Examination station skill training and mock OSCE practice with certified OSCE examiner feedback.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#234F81;margin-right:10px;"></span> BLOCK FOUR:<br> Procedural station skill training and mock OSCE practice with certified OSCE examiner feedback.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#649bc7;margin-right:10px;"></span> BLOCK FIVE:<br> Emergency station and sharps management skill development with certified OSCE examiner feedback.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#dde7ef;margin-right:10px;"></span> BLOCK SIX:<br> 6 station mock OSCE station practice with certified OSCE examiner feedback.<br><br>
                                <span style="width:10px;height:10px;display: inline-block;background-color:#234F81;margin-right:10px;"></span> BLOCK SEVEN:<br> Bespoke block - you can request your own training need.
                            </p>
                        </div>
                    
                        {{--<div class="col-md-5">--}}
                            {{--<div class="bespoke-datepicker" style="width: 100%;"></div>--}}
                            {{--<form class="col s12 package" action="/pay" method="POST">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<input name="type" value="bespoke" type="hidden">--}}
                                {{--<br>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="bespoke-date">Date</label>--}}
                                    {{--<input name="show-date" id="bespoke-show-date" type="text" class="validate form-control" disabled>--}}
                                    {{--<input name="date" id="bespoke-date" type="hidden" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="name">Name</label>--}}
                                    {{--<input name="name" id="name" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="email">Email</label>--}}
                                    {{--<input name="email" id="email" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="phone">Phone</label>--}}
                                    {{--<input name="phone" id="phone" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="message">Please list your training block requirements here:</label>--}}
                                    {{--<textarea style="min-height:250px;" name="user_message" id="message" type="text" class="validate form-control"></textarea>--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="price">Price</label>--}}
                                    {{--<input name="price" id="price" type="text" class="validate form-control" value="£129 per day" disabled>--}}
                                {{--</div>--}}
                                {{--<button type="submit" id="bespoke-submit" class="btn btn-primary btn-large waves-effect green darken-4 white-text right" disabled>Book</button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>

            <div id="group-service" style="display: block;border-color:#649bc7;" class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                        
                            {{--<h5 class="card-title">Group</h5>--}}
                            <p class="card-text">
                                The 2-day PA mock OSCE exam is an intensive workshop in Jan, May and September.<br>
                                I can provide the 2-day workshop in your local area during the week or on weekends for a minimum 10-person cohort.<br><br>
                                The 2-day group event would include:<br>

                                <br><span style="width:10px;height:10px;display: inline-block;background-color:#234F81;margin-right:10px;"></span>  DAY ONE:<br>
                                PA national exam preparation and marking scheme.<br>
                                Advanced communication and presentation skill training.<br>
                                Examination station skill training and practice.<br>

                                <br><span style="width:10px;height:10px;display: inline-block;background-color:#649bc7;margin-right:10px;"></span>  DAY TWO:<br>
                                Procedural station skills training.<br>
                                Emergency station and sharps management skills.<br>
                                Mock OSCE drill practice, individual assessment and feedback from
                                certified OSCE examiners.
                                
                            </p>
                        </div>
                    

                        {{--<div class="col-md-5">--}}
                            {{--<div class="group-datepicker" style="width: 100%;"></div>--}}
                            {{--<form class="col s12 package" action="/pay" method="POST">--}}
                                {{--{{ csrf_field() }}--}}
                                {{--<input name="type" value="group" type="hidden">--}}
                                {{--<br>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="bespoke-date">Date</label>--}}
                                    {{--<input name="show-date" id="group-show-date" type="text" class="validate form-control" disabled>--}}
                                    {{--<input name="date" id="group-date" type="hidden" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="name">Name</label>--}}
                                    {{--<input name="name" id="name" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="email">Email</label>--}}
                                    {{--<input name="email" id="email" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="phone">Phone</label>--}}
                                    {{--<input name="phone" id="phone" type="text" class="validate form-control">--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="message">Please list your training block requirements here: </label>--}}
                                    {{--<textarea style="min-height:250px;" name="user_message" id="message" type="text" class="validate form-control"></textarea>--}}
                                {{--</div>--}}
                                {{--<div class="input-field form-group form-text">--}}
                                    {{--<label for="price">Price</label>--}}
                                    {{--<input name="price" id="price" type="text" class="validate form-control" value="£229" disabled>--}}
                                {{--</div>--}}
                                {{--<button type="submit" id="group-submit" class="btn btn-primary btn-large waves-effect green darken-4 white-text right" disabled>Book</button>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>

            <div id="institution-service" style="display: none;" class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                        
                            <h5 class="card-title">Institution</h5>
                            <p class="card-text">
                                We are experienced clinical area and university lecturers.
                                We have experience in offering your institution a bespoke clinical
                                teaching or national OSCE preparation program for students or retakes. We are often invited to train at clinical and university institutions by
                                students as well as from lead lecturers. We can be booked to come to your institution and provide bespoke
                                training, please contact us and we can discuss how we can support your
                                institution cohort training or national OSCE preparation need.<br><br>

                                We can offer the following ‘blocks’ of training or OSCE practice drills to
                                your institution:<br><br>

                                BLOCK ONE– Advanced communication &amp; Hx taking skills<br>
                                - 6 station hx taking OSCE practice skill assessment and feedback<br><br>

                                Block TWO – Advanced communication &amp; patient examination skills<br>
                                - 6 station OSCE examination &amp; presentation assessment and feedback<br><br>

                                Block Three – Advanced presentation skills and procedural skills<br>
                                - 6 station OSCE procedural practice and feedback<br><br>

                                Block FOUR – Emergency drill practice and presentation skills<br>
                                - OSCE lifesaving and emergency drill practice and feedback<br><br>

                                Block Five - Advanced communication skills – giving bad news &amp; mental health assessment<br>
                                - 6 station OSCE communication skill practice and feedback<br><br>

                                Block six – ANTT / Cannulation, phlebotomy and procedural presentation skills practice<br>
                                - 6 station OSCE procedural skill practice and feedback<br><br>

                                Block seven – Advanced diagnostics practice and presentation skills practice<br>
                                - 6 station OSCE procedural skill and feedback
                            </p>
                            <h6>Negotiable</h6>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> 

    </section>

    <br><br>

    <div class="container">
        <div class="row">

            <div class="col-3">
                <a href="/images/1.jpg" class="swipebox" title="">
                    <img src="/images/1.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/2.jpg" class="swipebox" title="">
                    <img src="/images/2.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/3.jpg" class="swipebox" title="">
                    <img src="/images/3.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/4.jpg" class="swipebox" title="">
                    <img src="/images/4.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/5.jpg" class="swipebox" title="">
                    <img src="/images/5.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/6.jpg" class="swipebox" title="">
                    <img src="/images/6.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/6.jpg" class="swipebox" title="">
                    <img src="/images/6.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/7.jpg" class="swipebox" title="">
                    <img src="/images/7.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/8.jpg" class="swipebox" title="">
                    <img src="/images/8.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/9.jpg" class="swipebox" title="">
                    <img src="/images/9.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/10.jpg" class="swipebox" title="">
                    <img src="/images/10.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/11.jpg" class="swipebox" title="">
                    <img src="/images/11.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/12.jpg" class="swipebox" title="">
                    <img src="/images/12.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            {{--<div class="col-md-3">--}}
                {{--<a href="/images/13.jpg" class="swipebox" title="">--}}
                    {{--<img src="/images/13.jpg" alt="image" style="width:100%;">--}}
                {{--</a>--}}
            {{--</div>--}}

            <div class="col-3">
                <a href="/images/14.jpg" class="swipebox" title="">
                    <img src="/images/14.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/15.jpg" class="swipebox" title="">
                    <img src="/images/15.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/16.jpg" class="swipebox" title="">
                    <img src="/images/16.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/17.jpg" class="swipebox" title="">
                    <img src="/images/17.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/18.jpg" class="swipebox" title="">
                    <img src="/images/18.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/19.jpg" class="swipebox" title="">
                    <img src="/images/19.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/20.jpg" class="swipebox" title="">
                    <img src="/images/20.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/21.jpg" class="swipebox" title="">
                    <img src="/images/21.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/22.jpg" class="swipebox" title="">
                    <img src="/images/22.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/23.jpg" class="swipebox" title="">
                    <img src="/images/23.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/24.jpg" class="swipebox" title="">
                    <img src="/images/24.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/25.jpg" class="swipebox" title="">
                    <img src="/images/25.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/26.jpg" class="swipebox" title="">
                    <img src="/images/26.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/27.jpg" class="swipebox" title="">
                    <img src="/images/27.jpg" alt="image" style="width:100%;">
                </a>
            </div>

            <div class="col-3">
                <a href="/images/28.jpg" class="swipebox" title="">
                    <img src="/images/28.jpg" alt="image" style="width:100%;">
                </a>
            </div>

        </div>
    </div>

    <br><br>
    <section id="contact">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col col-lg-6">
          <header>
            <p style="color:#f9f9f9;text-align: center;">Please contact me by phone on 07932 750507 or by the contact form below on how I can help you.</p>
            <br>
          </header>

          <form class="col s12 package" action="/contact" method="POST">
            {{ csrf_field() }}
              <div class="row">

                @if(Session::has('contact_message'))
                    <div class="alert alert-success" style="width:100%;" role="alert">
                      Your contact form request has been sent!
                    </div>
                @endif

                @if($errors)
                  @foreach($errors->all() as $error)
                      <div class="alert alert-danger" style="width:100%;" role="alert">
                      {{ $error }}
                      </div>
                  @endforeach
                @endif

                <div class="input-field form-group form-text">
                    <label for="name">Name</label>
                    <input name="name" id="name" type="text" class="validate form-control">
                </div>

                <div class="input-field form-group form-text">
                    <label for="email">Email</label>
                    <input name="email" id="email" type="text" class="validate form-control">
                </div>

                <div class="input-field form-group form-text">
                    <label for="phone">Phone</label>
                    <input name="phone" id="phone" type="text" class="validate form-control">
                </div>
                
                <div class="input-field form-group form-text">
                    <label for="subject">Subject</label>
                    <input name="subject" id="subject" type="text" class="validate form-control">
                </div>

                <div class="input-field form-group form-text">
                    <label for="message">Message</label>
                    <textarea name="user_message" id="message" type="text" class="validate textarea form-control"></textarea>
                </div>
                
                <button type="submit" class="btn btn-primary btn-large waves-effect green darken-4 white-text right">Submit</button>
                
                <input name="uploader" id="uploader" type="hidden">

              </div>

          </form>
        </div>
      </div> <!-- /container -->
    </section>

    <footer class="container">
      <div class="copyright">
        <p>&copy; OSCE PASS Mentors 2019</p>
      </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <script src="/js/datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js" integrity="sha256-pXtSQrmprcTB74RsNlFHuJxHK5zXcPrOMx78uWU0ayU=" crossorigin="anonymous"></script>
    <script src="/js/jquery.swipebox.min.js"></script>


    <script type="text/javascript">

        $( '.swipebox' ).swipebox();

        $('.contact-scroll').click(function (e) {
            e.preventDefault();
            window.scrollTo(0,document.body.scrollHeight);
        });
    </script>
</body>
</html>
