You received a message from oscepassmentors.co.uk:

<p>
Name: {{ $name }}
</p>

<p>
Phone: {{ $phone }}
</p>

<p>
Email: {{ $email }}
</p>

<p>
Subject: {{ $subject }}
</p>

<p>
Message:<br><br> {{ $user_message }}
</p>