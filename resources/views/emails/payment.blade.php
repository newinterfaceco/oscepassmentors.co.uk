Dear {{ $name }},<br><br>

Thank you for registering for our {{ $type }} program!<br><br>

We have received your payment for £{{ $price }}. The payment has been authorized and approved.<br><br>

Your program date is: {{ $date }}<br><br>

Your Requirements:<br><br>
{{ $user_message }}<br><br>

If you have any questions or just want to discuss the program, feel free to reply to this email.<br><br>

We are looking forward to an exciting program ahead!<br><br>

Best regards,<br>
Steve.<br><br>

OSCE PASS MENTORS