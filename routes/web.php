<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('contact', 'ContactController@request');

Route::post('pay', 'ContactController@pay');

Route::get('{payment}/payment-success', 'ContactController@pay_success');

Route::get('{payment}/payment-error', 'ContactController@pay_error');

